#!/usr/bin/env python
# coding: utf-8

# In[2]:


import requests
from bs4 import BeautifulSoup


# In[3]:


#print("choppage du titre")
#total=soup.find("h1",{"class":"title"}).text
#print(total)
#totalnbr=int(total.split()[0])
#print("le n'ombre d'offre")
#print(totalnbr)
#str_range_page="page_"
#range_deb=0
#range_fin=19
#imax=int(totalnbr/19)
#print(imax)

#Cette partie utilise la liste des liens vers les pages avec 20 offres chacune et va renvoyer la liste des liens vers l'offre inviduelle

def creation_liste_lien(liste_page):

    liste_lien=[]

    for elt in liste_page:
        #print("voici le lien de la page")
        #print(elt)
    
        r = requests.get(elt)
        soup = BeautifulSoup(r.text, 'html.parser')
    
    #print(soup.prettify())
        a = soup.findAll("a", {"class" : "media with-fav"})
    
        for c in a:
        #print("voici le lien vers l'offre")
        #print(c.get("href"))  
            lien_complet="https://candidat.pole-emploi.fr"+c.get("href")
            liste_lien.append(lien_complet)
    
    print(f'liste_lien - terminé')
    return(liste_lien)


# In[ ]:




