#!/usr/bin/env python
# coding: utf-8

# In[11]:


import re
import numpy as np

import requests
from bs4 import BeautifulSoup
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.express as px
import lxml

from modules_python.pole_emploi.creation_liste_page import creation_liste_page
from modules_python.pole_emploi.creation_liste_lien import creation_liste_lien
from modules_python.pole_emploi.collecte_intitule import intitule
from modules_python.pole_emploi.collecte_date_offre import date_offre
from modules_python.pole_emploi.collecte_description import description
from modules_python.pole_emploi.collecte_type_contrat import type_contrat
from modules_python.pole_emploi.collecte_experience_offre import experience_offre
from modules_python.pole_emploi.collecte_ville_offre import ville_offre
from modules_python.pole_emploi.collecte_coordonnees_entreprise import coordonnees_entreprise
from modules_python.pole_emploi.collecte_infos_entreprise import infos_entreprise
from modules_python.pole_emploi.creation_dict_fonction import creation_dict_fonction


# In[ ]:




