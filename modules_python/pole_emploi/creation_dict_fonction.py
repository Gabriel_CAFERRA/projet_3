#!/usr/bin/env python
# coding: utf-8

# In[ ]:


import requests
from bs4 import BeautifulSoup

from modules_python.pole_emploi import creation_liste_page
from modules_python.pole_emploi import creation_liste_lien
from modules_python.pole_emploi import collecte_intitule
from modules_python.pole_emploi import collecte_date_offre
from modules_python.pole_emploi import collecte_description
from modules_python.pole_emploi import collecte_type_contrat
from modules_python.pole_emploi import collecte_experience_offre
from modules_python.pole_emploi import collecte_ville_offre
from modules_python.pole_emploi import collecte_coordonnees_entreprise
from modules_python.pole_emploi import collecte_infos_entreprise
from modules_python.pole_emploi import creation_dict_fonction


# In[ ]:


def creation_dict_fonction(liste_mots_Cle):
    dict_missing_coords = {}
    google_api_limit_current = 0
    marker = -1 # iterations count
    
    dico=dict()
    dico['motCle']=list()
    dico['lien_URL']=list()
    dico['intitulé']=list()
    dico['ville']=list()
    dico['date_publication']=list()
    dico['date_actualisation']=list()
    dico['numero_offre']=list()
    dico['description_offre']=list()
    dico['duree_contrat']=list()
    dico['type_contrat']=list()
    dico['experience']=list()
    dico['longitude']=list()
    dico['latitude']=list()
    dico['entreprise']=list()
    dico['url_entreprise']=list()
    
    for elt1 in liste_mots_Cle:
        
        liste_lien=creation_liste_lien(creation_liste_page(elt1))
        for elt2 in liste_lien:
#print(elt)
            marker += 1
            if marker%50 == 49:
                print('------------------------------------------')
                print('------------------------------------------')
                print(f'downloaded {marker + 1} offers this far')
                print('------------------------------------------')
                print('------------------------------------------')
    
    
            r = requests.get(elt2)
            
            if r.ok:
                dico['motCle'].append(elt1)
                dico['lien_URL'].append(elt2)
                soup = BeautifulSoup(r.text, 'html.parser')
                
                ##########################
                # Essai avec des fonctions
                #######################
                
                dico['intitulé'].append(collecte_intitule.intitule(soup))
                dicodateoffre = collecte_date_offre.date_offre(soup)
                if dicodateoffre[0] == "Publié":
                    dico['date_publication'].append(dicodateoffre[1])
                    dico['date_actualisation'].append(None)
                elif dicodateoffre[0] == "Actualisé":
                    dico['date_actualisation'].append(dicodateoffre[1])
                    dico['date_publication'].append(None)
                else:
                    dico['date_actualisation'].append(None)
                    dico['date_publication'].append(None)
                dico['numero_offre'].append(dicodateoffre[2])
                dico['description_offre'].append(collecte_description.description(soup))
                dicotypecontrat = collecte_type_contrat.type_contrat(soup)
                dico['duree_contrat'].append(dicotypecontrat[0])
                dico['type_contrat'].append(dicotypecontrat[1])
                dico['experience'].append(collecte_experience_offre.experience_offre(soup))
                dico['ville'].append(collecte_ville_offre.ville_offre(soup))
                dicocoord=collecte_coordonnees_entreprise.coordonnees_entreprise(soup,
                                                                                 dico['ville'][-1],
                                                                                 dict_missing_coords)
                dict_missing_coords = dicocoord[1]
                dico['longitude'].append(dicocoord[0][0])
                dico['latitude'].append(dicocoord[0][1])
                dicoentreprise=collecte_infos_entreprise.infos_entreprise(soup,google_api_limit_current)
                dico['entreprise'].append(dicoentreprise[0])
                dico['url_entreprise'].append(dicoentreprise[1])
                google_api_limit_current = dicoentreprise[2]
        
    return(dico)


# In[ ]:




