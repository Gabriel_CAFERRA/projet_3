#!/usr/bin/env python
# coding: utf-8

# In[15]:


import re

from bs4 import BeautifulSoup


# In[16]:


def experience_offre(soup):
    """
    In: soup
    Out: regexed experience column
    """
    pattern = re.compile(r'(?P<full>(?P<debutant>[Dd][eé]butant [aA]ccept[eé])'
                     r'|(?<!ï¿½ )(?<!\()(?P<annee>\d?\d)(?=( [aA]n\(?s?\)?)|( ï¿½ \d [Aa]n\(?s?\)?))'
                     r'|(?P<mois>\d?\d)(?= [mM]ois)'
                     r'|(?P<expabs>[Ee]xp[eé]rience [eE]xig[ée]e) ?\(?\d?\)?$)')

    experience = soup.find('span',{"class" : ["skill-name"]}).text
    regexed = re.search(pattern, experience)

    if regexed is not None:
        if regexed.group('expabs') is not None:
            choice = None
        elif regexed.group('debutant') == 'Débutant accepté':
            choice = round(0, 2)
        elif regexed.group('annee') is not None:
            choice = round(float(regexed.group('annee')), 2)
        elif regexed.group('mois') is not None:
            choice = round((float(regexed.group('mois')) / 12), 2)
    else:
        choice = None
        
    return choice


# In[ ]:




