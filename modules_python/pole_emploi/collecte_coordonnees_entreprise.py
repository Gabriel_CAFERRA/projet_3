#!/usr/bin/env python
# coding: utf-8

# In[1]:


import re

import requests
from bs4 import BeautifulSoup
from geopy.geocoders import Nominatim


# In[2]:


def coordonnees_entreprise(soup, ville, missing_coords_dict):
    dict_updated = missing_coords_dict
    listcoordo=[]
    avant_coordonnees = soup.find('ul', class_='list-unstyled action-secondary')
    if avant_coordonnees is not None:
        coordonnees = avant_coordonnees.find('a',href=re.compile('https://fr.mappy.com'))
#     try:
#         coordonnees = soup.find('ul', class_='list-unstyled action-secondary').find('a',href=re.compile('https://fr.mappy.com'))
#     except:
#         listcoordo.append(None)
#         listcoordo.append(None)
    else:
        listcoordo.append(None)
        listcoordo.append(None)
    if coordonnees is not None:
        lien_coordo= coordonnees.get('href')
        lien_mppy = requests.get(lien_coordo)
        if lien_mppy.ok:
#             print(lien_coordo, 'LIEN MAPPY')
            text_to_search = str(lien_mppy.text)
#             print('text_to_search\n', text_to_search)
            # Attention l'extreme ouest de la france a des longitudes négatives
            matching = re.search('"coordinates":{"lng":(-?[0-9.]+),"lat":(-?[0-9.]+)}', text_to_search)
            if matching is None:
                lng = None
                lat = None
            else:
                lng,lat = matching.groups()
            # ville = soup.find('span',itemprop="name")
            listcoordo.append(lng)
            listcoordo.append(lat)
    else:
        listcoordo.append(None)
        listcoordo.append(None)
    
    if None in listcoordo:
        if ville in missing_coords_dict.keys():
            listcoordo = missing_coords_dict[ville]
        else:
            if ville[-8:] == " (Dept.)":
                ville_mod = ville[:-8]
            else:
                ville_mod = ville
            geolocator = Nominatim(user_agent="P_3-G_3_collecte_offre_emploi")
            location = geolocator.geocode(ville_mod)
            if location is not None:
                listcoordo = [location.longitude, location.latitude]
            else:
                listcoordo = [None, None]
            dict_updated[ville] = listcoordo
    
    return(listcoordo, dict_updated)


# In[ ]:




