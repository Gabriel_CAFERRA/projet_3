#!/usr/bin/env python
# coding: utf-8

# In[2]:


from bs4 import BeautifulSoup


# In[3]:


def ville_offre(soup):
    ville = soup.find("span",{"itemprop":"name"}) 
    if ville is None:
        villedeloffre = None
    else:
        villedeloffre= ville.text
    return(villedeloffre)


# In[ ]:




