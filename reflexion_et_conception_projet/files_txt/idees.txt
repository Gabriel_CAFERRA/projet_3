Fusionner les scripts de chacun
Mettre en place une structure pour répartir le travail sur python via modules
Mettre en place des tests
Rafraichir quotidiennement les données disponibles - supprimer les offres qui ne sont plus disponibles
Compléter la collecte de données
Faire un modèle de base de données
Implémenter ce modèle de base de données
Créer d'autres visualisations
Explorer d'autres sites webs pour compléter le jeu de données
Faire du ml sur les descriptions pour extraire plus d'infos / des résumés d'offres
Faire des regex sur les descriptions pour extraire quelques infos sur les offres