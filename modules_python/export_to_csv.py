#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import psycopg2
from psycopg2.extensions import parse_dsn


# In[34]:


def export_to_csv():
    db_dsn = "postgres://postgres:test@localhost:5432/Offre_Emploi"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    df=pd.read_sql("""SELECT * FROM raw""",conn)
    df.to_csv('offres.csv', index=False)


# In[ ]:




